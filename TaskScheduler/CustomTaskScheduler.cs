﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TaskSchedulre
{
    class CustomTaskScheduler : TaskScheduler
    {
        private readonly ConcurrentQueue<Task> taskQueue = new ConcurrentQueue<Task>();

        protected override IEnumerable<Task> GetScheduledTasks() => taskQueue;

        protected override void QueueTask(Task task) => taskQueue.Enqueue(task);

        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            throw new NotImplementedException();
        }
    }
}
