﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TaskSchedulre
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var taskFactory = new CustomTaskFactory();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            await Task.WhenAll(
                taskFactory.Run(async () => await Task.Delay(1000)),
                taskFactory.Run(async () => await Task.Delay(1000)),
                taskFactory.Run(async () => await Task.Delay(1000)));

            sw.Stop();

            Print(sw.ElapsedMilliseconds);
        }

        static void Print(long ms, [CallerMemberName] string memberName = null)
        {
            Console.WriteLine($"Executing {memberName} took {ms} ms");
        }
    }
}
