﻿using System;
using System.Threading.Tasks;

namespace TaskSchedulre
{
    public class CustomTaskFactory : TaskFactory
    {
        public CustomTaskFactory(TaskScheduler taskScheduler = null)
            : base(taskScheduler ?? TaskScheduler.Default) { }

        public Task Run(Func<Task> func) => StartNew(func).Unwrap();
        public Task Run(Task task) => Run(task);

        public Task<T> Run<T>(Func<Task<T>> func) => StartNew(func).Unwrap();
        public Task<T> Run<T>(Task<T> task) => Run(() => task);
    }
}
